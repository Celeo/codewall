from codewall.shared import db
from datetime import datetime
from git import Repo


class Repository(db.Model):
    __tablename__ = 'codewall_repository'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)
    path = db.Column(db.String, unique=True)
    public = db.Column(db.Boolean, default=True)

    def __init__(self, name, path, public=True):
        self.name = name
        self.path = path
        self.public = public

    @property
    def git_repo(self):
        return Repo(self.path)

    def get_commit_object(self, commit, branch='master'):
        repo = self.git_repo
        if commit == 'HEAD':
            return repo.branches[branch].commit
        else:
            branch_commits = list(repo.iter_commits(rev=repo.branches[branch]))
            return [c for c in branch_commits if c.hexsha == commit][0]

    def files(self, path='', commit='HEAD', branch='master'):
        repo = self.git_repo
        at_commit = self.get_commit_object(commit, branch=branch)
        if not path:
            items = at_commit.tree.traverse(depth=1)
        else:
            path_item = at_commit.tree[path]
            if path_item.type == 'blob':
                return []
            items = list(path_item.traverse(depth=1))
        return sorted([CodeFile(repo, branch, item) for item in items], key=lambda x: not x.is_directory)

    def branches(self):
        return Repo(self.path).branches

    def commits(self, branch='master', offset=0, per_page=20):
        offset, per_page = int(offset), int(per_page)
        return list(Repo(self.path).iter_commits(branch, max_count=per_page, skip=offset * per_page))

    def commit_count(self, branch='master'):
        return len(list(Repo(self.path).iter_commits(branch)))


class CodeFile:

    def __init__(self, repo, branch, item):
        self.item = item
        self.last_commit = repo.iter_commits(paths=item.path, rev=branch, max_count=1).__next__()
        self.last_commit_author = self.last_commit.author
        self.last_commit_date = datetime.fromtimestamp(self.last_commit.authored_date).strftime('%m/%d/%Y @ %I:%M:%S')

    @property
    def is_directory(self):
        return self.item.type == 'tree'

    @property
    def name(self):
        if self.item.type == 'tree':
            return '%s/' % self.item.name
        return self.item.name

    @property
    def path(self):
        return self.item.path
