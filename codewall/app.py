from flask import Flask, render_template, abort, Markup, request, session, url_for, redirect
from codewall.shared import db
from codewall.models import Repository
from datetime import datetime


app = Flask(__name__)
app.config.from_pyfile('config.cfg')
db.app = app
db.init_app(app)


@app.route('/')
def index():
    return render_template('index.html', repositories=Repository.query.filter_by(public=True).all())


@app.route('/repo/<repo_name>')
@app.route('/repo/<repo_name>/browse/<path:code_path>')
def repo(repo_name, code_path=''):
    commit_offset = request.args.get('offset', 0)
    branch = session.get('%s-branch' % repo_name, 'master')
    commit = session.get('%s-commit' % repo_name, 'HEAD')
    show_tab = request.args.get('showtab', 'code')
    repo = Repository.query.filter_by(name=repo_name).first()
    if not repo:
        abort(404)
    return render_template('repo.html', repo=repo, code_path=code_path,
        commit_offset=commit_offset, show_tab=show_tab, branch=branch, commit=commit)


@app.route('/repo/<repo_name>/branch')
def switch_branch(repo_name):
    branch = request.args.get('branch', 'master')
    session['%s-branch' % repo_name] = branch
    session_commit_key = '%s-commit' % repo_name
    if session_commit_key in session:
        session.pop(session_commit_key)
    return redirect(url_for('repo', repo_name=repo_name))


@app.route('/repo/<repo_name>/inspect/<path:code_path>')
def inspect(repo_name, code_path):
    repo = Repository.query.filter_by(name=repo_name).first()
    if not repo:
        abort(404)
    branch = session.get('%s-branch' % repo_name, 'HEAD')
    commit = session.get('%s-commit' % repo_name, 'HEAD')
    commit_obj = repo.get_commit_object(commit, branch=branch)
    git_repo = repo.git_repo
    f = git_repo.tree(git_repo.commit(commit_obj))[code_path]
    data = f.data_stream.read()
    if isinstance(data, bytes):
        data = data.decode('UTF-8')
    up_directory = code_path[:code_path.rfind('/')] if '/' in code_path else None
    return render_template('inspect.html', repo=repo, code_path=code_path,
        up_directory=up_directory, data=data, commit=commit_obj)


@app.route('/repo/<repo_name>/<commit_sha>')
def commit(repo_name, commit_sha):
    repo = Repository.query.filter_by(name=repo_name).first()
    if not repo:
        abort(404)
    commit = repo.git_repo.commit(commit_sha)
    all_commits = list(repo.git_repo.iter_commits(rev=session.get('%s-branch' % repo_name, 'master')))
    current_commit_index = all_commits.index(commit)
    previous_commit, next_commit = None, None
    if current_commit_index + 1 < len(all_commits):
        previous_commit = all_commits[current_commit_index + 1]
    if current_commit_index > 0:
        next_commit = all_commits[current_commit_index - 1]
    diff_raw = Markup.escape(repo.git_repo.git.show(commit))
    diff, color = '', None
    search_line = [l for l in diff_raw.splitlines() if l.startswith('diff --git a/')][0]
    start = diff_raw.splitlines().index(search_line)
    for line in diff_raw.splitlines()[start:]:
        if line.startswith('+++') or line.startswith('---'):
            continue
        elif line.startswith('+'):
            color = 'add'
        elif line.startswith('-'):
            color = 'remove'
        elif line.startswith('diff --git'):
            color = 'divider'
        elif line.startswith('@@') or line.startswith('index ') or \
                line.startswith('deleted file mode ') or line.startswith('new file mode'):
            color = 'info'
        else:
            color = 'none'
        diff += '<div class="code_row diff_%s">%s</div>' % (color, line)
    return render_template('commit.html', repo=repo, commit=commit,
        previous_commit=previous_commit, next_commit=next_commit, diff=diff)


@app.route('/repo/<repo_name>/commit')
def switch_commit(repo_name):
    commit = request.args.get('commit', 'master')
    session['%s-commit' % repo_name] = commit
    return redirect(url_for('repo', repo_name=repo_name))


@app.errorhandler(404)
def error_404(error):
    return render_template('404.html')


@app.errorhandler(500)
def error_500(error):
    return render_template('500.html')


@app.template_filter('date')
def filter_date(s):
    return datetime.fromtimestamp(int(s)).strftime('%b %d, %Y at %I:%M:%S')


@app.template_filter('sha')
def filter_sha(s):
    return s[:7]


@app.template_filter('split_path')
def filter_split_path(s):
    class Directory:
        def __init__(self, full, short, current=False):
            self.full = full
            self.short = short
            self.current = current

        def __repr__(self):
            return '%s-%s-%s' % (self.full, self.short, self.current)

    crumbs = []
    spl = s.split('/')
    for index, segment in enumerate(spl):
        crumbs.append(Directory('/'.join(spl[:index + 1]), segment))
    crumbs[-1].current = True
    ret = crumbs if len(crumbs) > 0 and crumbs[0].short else []
    return ret


@app.template_filter('commit_range')
def filter_commit_range(i):
    return list(range(1, i + 2))


@app.template_filter('remove_master')
def filter_remove_master(l):
    return [i for i in l if not i.name == 'master']
